# LEMON fixes, updates and new features



### Version 1.3.2 (updated from v1.3.1)

---

### 2021-11-07 Fix macOS compatibility for CPlex

Error message:
Undefined symbols for architecture x86_64: "lemon::LpBase::INF" - ld: symbol(s) not found for architecture x86_64

Changes:

- `deps/lemon/cmake/FindILOG.cmake`

```
new line 69:
${ILOG_CPLEX_ROOT_DIR}/lib/x86-64_osx/static_pic

new line 82:
${ILOG_CONCERT_ROOT_DIR}/lib/x86-64_osx/static_pic
```
