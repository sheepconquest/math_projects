## Math projects

Az alábbi parancsok a projekt root könyvtárából indíthatók

```
make help
```

---

Szükség esetén létrehozza a `build` mappát és legenerálja a fájlokat, majd build-eli a `dijkstra`,
`fas-cplex` és a `three-fas`-et
```
make
```

Szükség esetén létrehozza a `build` mappát és legenerálja a fájlokat
```
make generate
```

Build-eli a `dijkstra`-t
```
make dijkstra
```

Build-eli az `fas-cplex`-et
```
make fas-cplex
```

Build-eli az `three-fas`-et
```
make three-fas
```

Kiüríti a `build` mappát
```
make clean
```

---

### test

dijkstra - upd 2022-04-23: a path nem jó, nincs ilyen mappa jelenleg
```
make dijkstra_test
/Users/keli/Documents/CLionProjects/math_projects/deps/lemon/build/test/dijkstra_test
```
