.PHONY: all clean generate dijkstra fas-cplex three-fas help

CM := cmake
CM_COLOR := cmake -E cmake_echo_color --switch=$(COLOR) --cyan
#BUILD_ARGS := -DLEMON_DEFAULT_LP=CPLEX -DILOG_ROOT_DIR=/Applications/CPLEX_Studio201
NOT_EMPTY := $(shell ls -A build)

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

all: generate dijkstra fas-cplex three-fas

clean:
	@${CM_COLOR} "Cleaning build directory..."
ifeq ($(NOT_EMPTY),)
	@echo "Directory is empty"
else
	@rm -r build/*
endif

generate:
	@${CM_COLOR} "Generating files to build directory..."
	@${CM} -S . -B build

dijkstra:
	@${CM_COLOR} "Building dijkstra project to build directory..."
	@$(MAKE) $(MAKESILENT) -C build dijkstra

fas-cplex:
	@${CM_COLOR} "Building fas-cplex project to build directory..."
	@$(MAKE) $(MAKESILENT) -C build fas-cplex

three-fas:
	@${CM_COLOR} "Building three-fas project to build directory..."
	@$(MAKE) $(MAKESILENT) -C build three-fas

help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... generate"
	@echo "... dijkstra"
	@echo "... fas-cplex"
	@echo "... three-fas"
