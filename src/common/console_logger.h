#ifndef MATH_PROJECTS_CONSOLE_LOGGER_H
#define MATH_PROJECTS_CONSOLE_LOGGER_H

enum Log {
    NoLog, Info, Debug
};

namespace log {

    Log getLog(const std::string &arg) {
        if ((arg == "info") || (arg == "INFO")) {
            return Info;
        }

        if ((arg == "debug") || (arg == "DEBUG")) {
            return Debug;
        }

        return NoLog;
    }
}


bool isLog(const Log &level, const Log &log);

Log getLog(const int &argc, const char *argv[]);

bool isInfo(const Log &log) {
    return isLog(Info, log);
}

bool isDebug(const Log &log) {
    return isLog(Debug, log);
}

bool isLog(const Log &level, const Log &log) {
    switch (level) {
        case Info:
            return log == Info || log == Debug;
        case Debug:
            return log == Debug;
        case NoLog:
        default:
            return false;
    }
}

Log getLog(const int &argc, const char *argv[]) {
    if (argc == 1) {
        return NoLog;
    }

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];

        if ((arg == "-l") || (arg == "--log")) {
            if (i + 1 < argc) {
                return log::getLog(argv[++i]);
            }
        }
    }

    return NoLog;
}

#endif //MATH_PROJECTS_CONSOLE_LOGGER_H
