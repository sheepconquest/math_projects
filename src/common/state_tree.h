#ifndef MATH_PROJECTS_STATE_TREE_H
#define MATH_PROJECTS_STATE_TREE_H

#include <queue>
#include <lemon/bin_heap.h>
#include <lemon/smart_graph.h>
#include "console_logger.h"
#include "weight_map.h"

using namespace lemon;

typedef std::vector<SmartDigraph::Node> NodeList;// TODO: ez nem kéne ide
typedef std::vector<const SmartDigraph::Node *> NodePtrList;
typedef std::vector<const SmartDigraph::Node *> PtrToConstNodeList;
typedef std::vector<SmartDigraph::Node *const> ConstPtrToNodeList;

int getId(const SmartDigraph::Node &node);

int getId(const SmartDigraph::Node *node);


class StateTreeBase {
protected:
    const Log _log;
    const int _id;
    StateTreeBase *const _parent;// const ptr to StateTreeBase
    const SmartDigraph::Node *_root;// ptr to const SmartDigraph::Node
    int _weight;
    int _level;
    NodePtrList _nodes;

    StateTreeBase(int id, StateTreeBase *parent, const SmartDigraph::Node *&root, const NodePtrList &nodes,
                  int weight, int level)
            : StateTreeBase(id, parent, root, nodes, weight, level, NoLog) {}

    StateTreeBase(int id, StateTreeBase *parent, const SmartDigraph::Node *&root, const NodePtrList &nodes,
                  int weight, int level, const Log &log)
            : _id(id), _parent(parent), _root(root), _weight(weight), _level(level), _log(log) {
        setNodes(root, nodes);
    }

    // TODO: remove
    SmartDigraph::Node *getNextNode(const WeightMap &weights, int &nextWeight) {
        int poz = 0;
        auto *next = const_cast<SmartDigraph::Node *>(_nodes[poz]);
        int minWeight = _weight + weights.getWeight(*_root, *_nodes[poz]);

        for (int i = 1; i < _nodes.size(); ++i) {
            int w = _weight + weights.getWeight(*_root, *_nodes[i]);
            if (w < minWeight) {
                next = const_cast<SmartDigraph::Node *>(_nodes[i]);
                minWeight = w;
                poz = i;
            }
        }

        nextWeight = minWeight;

        if (isDebug(_log)) {
            std::cout << "getNextNode(): remove node [" << getId(_nodes[poz]) << "] from position "
                      << poz + 1 << std::endl;
            std::cout << "getNextNode(): weight of the next node: " << nextWeight << std::endl;
        }
        _nodes.erase(_nodes.begin() + poz);

        return next;
    }

private:
    void setNodes(const SmartDigraph::Node *&root, const NodePtrList &source) {
        NodePtrList remainingNodes;
        remainingNodes.reserve(source.size() - 1);

        std::for_each(source.begin(), source.end(), [&](const SmartDigraph::Node *node) {
            if (*node != *root) {
                remainingNodes.push_back(node);
            }
        });

        if (isDebug(_log)) {
            std::cout << "setNodes(): filtered node pointer list created with size: " << remainingNodes.size()
                      << std::endl;
            std::for_each(remainingNodes.begin(), remainingNodes.end(), [&](const SmartDigraph::Node *&node) {
                std::cout << " " << getId(node);
            });
            std::cout << std::endl;
        }

        _nodes = remainingNodes;
    }

protected:
    int getWeight() const {
        return _weight;
    }

    int getLevel() const {
        return _level;
    }

public:
    StateTreeBase *getParent() const {
        return _parent;
    }

    const SmartDigraph::Node *getRoot() const {
        return _root;
    }

    class Comparator {
    public:
        int operator()(StateTreeBase *t1, StateTreeBase *t2) {
            if (t1->getWeight() == t2->getWeight()) {
                return t1->getLevel() < t2->getLevel();
            }
            return t1->getWeight() > t2->getWeight();
        }
    };
};

class StateTree : public StateTreeBase {

    // FIXME: csak ennyi...
//    typedef int Key;
//    typedef std::map<int, StateTreeBase*> HeapMap;
//    typedef typename HeapMap::key_type Key;
//    typedef concepts::ReadWriteMap<int, StateTreeBase*> HeapMap;
//    typedef BinHeap<int, HeapMap, Comparator> ProcessHeap;

public:
    class StateSubTree : public StateTreeBase {
        typedef std::priority_queue<StateSubTree *, std::vector<StateSubTree *>, Comparator> Queue;

        friend class StateTree;// TODO: ez lehet felesleges

        std::vector<StateSubTree *> _children;

    public:
        StateSubTree(int id, StateTreeBase *parent, const SmartDigraph::Node *root, const NodePtrList &nodes,
                     int weight, int level)
                : StateTreeBase(id, parent, root, nodes, weight, level + 1) {
        }

        StateSubTree(int id, StateTreeBase *parent, const SmartDigraph::Node *root, const NodePtrList &nodes,
                     int weight, int level, const Log &log)
                : StateTreeBase(id, parent, root, nodes, weight, level + 1, log) {

            if (isDebug(log)) std::cout << toString() << std::endl << std::endl;
        }

        virtual ~StateSubTree() {
            if (isDebug(_log)) std::cout << "~" << toString() << std::endl;

            _nodes.clear();

            while (!_children.empty()) {
                StateSubTree *subTree = _children.back();
                delete subTree;
                _children.pop_back();
            }
        }

        // TODO: TEST: return + null check || clear + rewrite?
        void process(const WeightMap &weights, Queue &inProgress, NodePtrList &currFAS, int &currOpt, int &counter) {
//        NodePtrList process(const WeightMap &weights, Queue &inProgress, int &currOpt, int &counter) {
            if (_nodes.size() == 2) {
                if (isDebug(_log)) {
                    std::cout << "process(): processing last 2 nodes: ("
                              << getId(_root) << "->" << getId(_nodes.front()) << ") ("
                              << getId(_root) << "->" << getId(_nodes.back()) << ")" << std::endl;
                }
                processLastTwoNodes(weights, currFAS, currOpt);
                return;
//                return processLastTwoNodes(weights, currOpt);
            }

            std::vector<StateSubTree *> newTrees;
            for (auto &node: _nodes) {
                if (isDebug(_log)) {
                    std::cout << "process(): processing node: (" << getId(_root) << "->" << getId(node) << ")" << std::endl;
                }

                int nextWeight = getWeight(weights, node);

                if (nextWeight < currOpt) {
                    auto *subTree = new StateSubTree(counter++, this, node,
                                                     _nodes, nextWeight, _level, _log);

//                    inProgress.push(subTree);
                    newTrees.push_back(subTree);
                    _children.push_back(subTree);
                } else if (nextWeight > currOpt) {
                    if (isInfo(_log)) {
                        if (isDebug(_log)) {
                            std::cout << "process(): [" << getId(node) << "]: (" << nextWeight << ">" << currOpt << ")"
                                      << std::endl;
                        }
                        std::cout << "Cutting node [" << getId(_root) << "]" << std::endl;
                        if (isDebug(_log)) std::cout << std::endl;
                    }

                    destroyChildren();
                    return;
                }
            }

            std::for_each(newTrees.begin(), newTrees.end(), [&](StateSubTree *subTree) {
                inProgress.push(subTree);
            });

//            NodePtrList result;
//            return result;
        }

        std::string toString() const {
            return "StateSubTree(id=" + std::to_string(_id) + ", root=" + std::to_string(getId(_root)) +
                   ", weight=" + std::to_string(_weight) + ", level=" + std::to_string(_level) + ")";
        }

    private:
        void processLastTwoNodes(const WeightMap &weights, NodePtrList &currFAS, int &currOpt) {
//        NodePtrList processLastTwoNodes(const WeightMap &weights, int &currOpt) {
            const SmartDigraph::Node* node1 = _nodes.front();
            const SmartDigraph::Node* node2 = _nodes.back();

            int weightNode1 = getWeight(weights, node1);
            int weightNode2 = getWeight(weights, node2);

            int sumWeight = weightNode1 + weightNode2 - _weight;

            int wFrontFirst = weights.getWeight(*node1, *node2);
            int wBackFirst = weights.getWeight(*node2, *node1);

            bool frontFirst = true;
            if (wFrontFirst <= wBackFirst) {
                sumWeight += wFrontFirst;
            } else {
                sumWeight += wBackFirst;
                frontFirst = false;
            }

            // void version
            currFAS.clear();
            if (sumWeight < currOpt) {
                if (frontFirst) {
                    currFAS.push_back(node2);
                    currFAS.push_back(node1);
                } else {
                    currFAS.push_back(node1);
                    currFAS.push_back(node2);
                }

                createFAS(currFAS);

                if (isInfo(_log)) {
                    std::cout << "New optimal solution found, previous value [" << currOpt << "] updated with ["
                              << sumWeight << "]" << std::endl;
                    std::cout << "Nodes of the Feedback Arc Set:" << std::endl;

                    std::reverse(currFAS.begin(), currFAS.end());
                    std::for_each(currFAS.begin(), currFAS.end(), [&](const SmartDigraph::Node *node) {
                        std::cout << " " << getId(node);
                    });

                    std::cout << std::endl << std::endl;
                }

                currOpt = sumWeight;
            } else {
                if (isDebug(_log)) {
                    std::cout << "processLastTwoNodes(): no update: (" << sumWeight << ">" << currOpt << ")"
                              << std::endl << std::endl;
                }
            }

//            NodePtrList result;
//            if (sumWeight < currOpt) {
//                if (frontFirst) {
//                    result.push_back(node2);
//                    result.push_back(node1);
//                } else {
//                    result.push_back(node1);
//                    result.push_back(node2);
//                }
//
//                createFAS(result);
//
//                if (isInfo(_log)) {
//                    std::cout << "New optimal solution found, previous value [" << currOpt << "] updated with ["
//                              << sumWeight << "]" << std::endl;
//                    std::cout << "Nodes of the Feedback Arc Set:" << std::endl;
//
//                    std::reverse(result.begin(), result.end());
//                    std::for_each(result.begin(), result.end(), [&](const SmartDigraph::Node *node) {
//                        std::cout << " " << getId(node);
//                    });
//
//                    std::cout << std::endl << std::endl;
//                }
//
//                currOpt = sumWeight;
//            }
//
//            return result;
        }

        int getWeight(const WeightMap &weights, const SmartDigraph::Node *nextNode) {
            int weight = _weight + weights.getWeight(*_root, *nextNode);

            if (isDebug(_log)) std::cout << "getWeight(): weights backwards to previous nodes:";

            StateTreeBase *parent = getParent();
            while (parent != nullptr) {
                if (isDebug(_log)) {
                    int previousWeight = weights.getWeight(*(parent->getRoot()), *nextNode);
                    std::cout << " (" << getId(parent->getRoot()) << "<-" << getId(nextNode) << ")="
                              << previousWeight;
                }

                weight += weights.getWeight(*(parent->getRoot()), *nextNode);

                parent = parent->getParent();
            }

            if (isDebug(_log)) std::cout << std::endl;

            return weight;
        }

        void createFAS(NodePtrList &list) {
            list.push_back(_root);

            StateTreeBase *parent = getParent();
            while (parent != nullptr) {
                list.push_back(parent->getRoot());

                parent = parent->getParent();
            }
        }

        void destroyChildren() {
            while (!_children.empty()) {
                StateSubTree *subTree = _children.back();
                delete subTree;
                _children.pop_back();
            }
        }
    };

private:
    typedef std::priority_queue<StateSubTree *, std::vector<StateSubTree *>, Comparator> Queue;

    int _counter = 1;
    int _currentOptWeight;// result
    NodePtrList _fasNodes;// result
    std::vector<StateSubTree *> _children;
//    std::vector<StateSubTree *> _inProgress;// az ágak utolsó, feldolgozandó eleme
//    ProcessHeap _heap(HeapMap());
    Queue _inProgress;

public:
    StateTree(const SmartDigraph::Node *root, const NodePtrList &nodes, int startOpt)
            : StateTreeBase(0, nullptr, root, nodes, 0, 0), _currentOptWeight(startOpt) {
//        _fasNodes.push_back(root);// TODO: lehet ez se kell ide, nem sorban töltöm, hanem a végén fogom egyben
//        HeapMap heapMap;
//        ProcessHeap _heap(heapMap);
    }

    StateTree(const SmartDigraph::Node *root, const NodePtrList &nodes, int startOpt, const Log &log)
            : StateTreeBase(0, nullptr, root, nodes, 0, 0, log), _currentOptWeight(startOpt) {
//        _fasNodes.push_back(root);
//        HeapMap heapMap;
//        ProcessHeap _heap(heapMap);

        if (isDebug(log)) std::cout << toString() << std::endl << std::endl;
    }

    virtual ~StateTree() {
        if (isDebug(_log)) std::cout << "~" << toString() << std::endl;

        _nodes.clear();

        while (!_children.empty()) {
            StateSubTree *subTree = _children.back();
            delete subTree;
            _children.pop_back();
        }
    }

    void process(const WeightMap &weights) {
        init(weights);

        // TODO: majd, ha kész a folyamat vége is
        while (!_inProgress.empty()) {
            StateSubTree *next = _inProgress.top();
            _inProgress.pop();

            if (isInfo(_log)) {
                std::cout << "Processing node: " << next->toString() << std::endl;
                if (isDebug(_log)) std::cout << std::endl;
            }
            next->process(weights, _inProgress, _fasNodes, _currentOptWeight, _counter);
        }

//        StateSubTree *next = _inProgress.top();
//        _inProgress.pop();
//
//        // TODO: TEST: return + null check || clear + rewrite?
//        std::cout << "Processing node: " << next->toString() << std::endl << std::endl;
//        next->process(weights, _inProgress, _fasNodes, _currentOptWeight, _counter);
////        NodePtrList result = next->process(weights, _inProgress, _currentOptWeight, _counter);
    }

    int getOptWeight() const {
        return _currentOptWeight;
    }

    const NodePtrList &getFasNodes() const {
        return _fasNodes;
    }

    std::string toString() const {
        return "StateTree(id=" + std::to_string(_id) + ", root=" + std::to_string(getId(_root)) +
               ", weight=" + std::to_string(_weight) + ", level=" + std::to_string(_level) + ")";
    }

private:
    void init(const WeightMap &weights) {
        for (auto &nextNode: _nodes) {
            int nextWeight = weights.getWeight(*_root, *nextNode);

            if (nextWeight < _currentOptWeight) {
                auto *subTree = new StateSubTree(_counter++, this, nextNode,
                                                 _nodes, nextWeight, _level, _log);
                _inProgress.push(subTree);
                _children.push_back(subTree);

                // TODO: mindkettő másol, az eredeti megmarad
//                std::cout << "1: " << &subTree << std::endl;
//                _inProgress.push(subTree);
//                auto *p1 = _inProgress.top();
//                std::cout << "2: " << &p1 << std::endl;
//                std::cout << "3: " << &subTree << std::endl;
//                _children.push_back(subTree);
//                auto *p2 = _children.back();
//                std::cout << "4: " << &p2 << std::endl;
//                std::cout << "5: " << &subTree << std::endl;
            }
        }
    }
};


int getId(const SmartDigraph::Node node) {
    return SmartDigraph::id(node) + 1;
}

int getId(const SmartDigraph::Node *node) {
    return SmartDigraph::id(*node) + 1;
}

#endif //MATH_PROJECTS_STATE_TREE_H
