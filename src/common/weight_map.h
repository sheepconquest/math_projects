#ifndef MATH_PROJECTS_WEIGHT_MAP_H
#define MATH_PROJECTS_WEIGHT_MAP_H

#include <lemon/smart_graph.h>

using namespace lemon;

class WeightMap {// TODO: update: láncolt lista (target -> s1, s2, ...)
private:
    class FeedbackArcWeight {
        friend class WeightMap;

        SmartDigraph::Node _target;
        SmartDigraph::Node _source;
        int _weight;

    protected:
        FeedbackArcWeight(const SmartDigraphBase::Node &target, const SmartDigraphBase::Node &source, int weight)
                : _target(target), _source(source), _weight(weight) {}

//        bool operator==(const FeedbackArcWeight &rhs) const {
//            return _target == rhs._target &&
//                   _source == rhs._source;
//        }
//
//        bool operator!=(const FeedbackArcWeight &rhs) const {
//            return !(rhs == *this);
//        }

        bool match(const SmartDigraph::Node target, const SmartDigraph::Node source) const {
            return _target == target && _source == source;
        }

        int getWeight() const {
            return _weight;
        }

        std::string toString() const {
            return "(" + std::to_string(SmartDigraph::id(_target) + 1) + "<-"
                   + std::to_string(SmartDigraph::id(_source) + 1) + ")=" + std::to_string(_weight);
        }

    public:
        virtual ~FeedbackArcWeight() = default;
    };

    std::vector<FeedbackArcWeight> _weights;

public:
    WeightMap(const SmartDigraph &g, const SmartDigraph::ArcMap<int> &arcMap) {
        _weights.reserve(g.arcNum());

        std::for_each(g.arcs().begin(), g.arcs().end(), [&](const SmartDigraph::Arc &arcIt) {
            int w = arcMap[arcIt];
            SmartDigraph::Node target = g.target(arcIt);
            SmartDigraph::Node source = g.source(arcIt);

            FeedbackArcWeight faw(target, source, w);
            _weights.push_back(faw);
        });
    }

    virtual ~WeightMap() {
        _weights.clear();
    }

    int getWeight(const SmartDigraph::Node target, const SmartDigraph::Node source) const {
        for (const auto &_weight: _weights) {
            if (_weight.match(target, source)) {
                return _weight.getWeight();
            }
        }
        return 0;
    }

    std::string toString() const {
        std::stringstream ss;
        ss << "FeedbackArcWeights:";
        std::for_each(_weights.begin(), _weights.end(), [&](const FeedbackArcWeight &faw) {
            ss << " " << faw.toString();
        });

        return ss.str();
    }
};

#endif //MATH_PROJECTS_WEIGHT_MAP_H
