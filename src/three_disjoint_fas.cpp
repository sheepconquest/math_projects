#include <lemon/lp.h>
#include <lemon/smart_graph.h>
#include <lemon/lgf_reader.h>
#include <lemon/connectivity.h>
#include <set>
#include "common/console_logger.h"

using namespace lemon;

int threeDisjointFAS(const SmartDigraph &g, const Log &log = Info);

void triangleInequalities(const Log &log, Mip &mip, std::vector<std::vector<Mip::Col> > &y, int n,
                          std::string (*writer) (int, int));

void init(SmartDigraph &g, int nodeSize, const Mip &mip, const std::vector<std::vector<Mip::Col> > &y);
void init(const SmartDigraph &sourceGraph, SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map);
void addArc(SmartDigraph &g, int sourceNode, int targetNode);
void addArc(SmartDigraph &g, SmartDigraph::Node &source, SmartDigraph::Node &target);
bool isDAG(const Log &log, SmartDigraph &g, const std::string &graphName = "G'");
void getArcsForTheFAS(const Log &log, const SmartDigraph &g, std::vector<SmartDigraph::Arc> &fas,
                      const SmartDigraph::NodeMap<int> &topOrder);
bool isDisjoint(const std::vector<SmartDigraph::Arc> &fas1, const std::vector<SmartDigraph::Arc> &fas2,
                const std::vector<SmartDigraph::Arc> &fas3);
bool isFAS(const Log &log, const SmartDigraph &g, const std::vector<SmartDigraph::Arc> &fas);

std::string wr_y(int i, int j);
std::string wr_y2(int i, int j);
std::string wr_y3(int i, int j);
void wr_y(const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int));
void wr_sol(Mip &mip, const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int),
            bool upperTriangMtx = false);
void wr_topOrder(const SmartDigraph::NodeMap<int> &topOrder, int n, const std::string &graphName = "G'");
void wr_arcMap(const SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map,
               const std::string &setName);

int main(int argc, const char* argv[]) {
    Log log = getLog(argc, argv);

    DIGRAPH_TYPEDEFS(SmartDigraph);

    if (argc < 3) {
        return -1;
    }

    std::string argStr1(argv[1]);
    std::string argStr2(argv[2]);

    SmartDigraph g;
    try {
        digraphReader(g, argStr1 + "/" + argStr2)
                .run();
    } catch (Exception &error) {
//        std::cerr << "Error: " << error.what() << std::endl;
        return -1;
    }

    return threeDisjointFAS(g, log);
}

int threeDisjointFAS(const SmartDigraph &g, const Log &log) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    if (isLog(Debug, log)) {
        std::cout << std::endl << "--- Finding 3 disjoint feedback arc sets by MIP solving with CPLEX ---"
                  << std::endl << std::endl;
    }

    // TODO: még lehet jól jön
//    if (dag(g)) {
//        return -1;
//    }

    Mip mip;
    std::vector<std::vector<Mip::Col> > y1;
    std::vector<std::vector<Mip::Col> > y2;
    std::vector<std::vector<Mip::Col> > y3;

    int n = g.nodeNum();

    if (isLog(Debug, log)) {
        std::cout << "Initializing Mip::Col elements and adding to the 3 y matrices" << std::endl;
    }
    for (int i = 0; i < n; ++i) {
        std::vector<Mip::Col> row1;
        std::vector<Mip::Col> row2;
        std::vector<Mip::Col> row3;

        for (int j = 0; j < n; ++j) {
            Mip::Col col1 = mip.addCol();
            mip.colLowerBound(col1, 0);
            mip.colUpperBound(col1, 1);
            mip.colType(col1, Mip::INTEGER);

            Mip::Col col2 = mip.addCol();
            mip.colLowerBound(col2, 0);
            mip.colUpperBound(col2, 1);
            mip.colType(col2, Mip::INTEGER);

            Mip::Col col3 = mip.addCol();
            mip.colLowerBound(col3, 0);
            mip.colUpperBound(col3, 1);
            mip.colType(col3, Mip::INTEGER);

            row1.push_back(col1);
            row2.push_back(col2);
            row3.push_back(col3);
        }

        y1.push_back(row1);
        y2.push_back(row2);
        y3.push_back(row3);
    }

    if (isLog(Debug, log)) {
        wr_y(y1, n, wr_y);
        wr_y(y2, n, wr_y2);
        wr_y(y3, n, wr_y3);
    }

    if (isLog(Debug, log)) {
        std::cout << "Constraint: triangle inequalities" << std::endl;
    }
    triangleInequalities(log, mip, y1, n, &wr_y);
    triangleInequalities(log, mip, y2, n, &wr_y2);
    triangleInequalities(log, mip, y3, n, &wr_y3);

    if (isLog(Debug, log)) {
        std::cout << "Constraint: disjoint sets" << std::endl;
    }
    for (ArcIt it(g); it != INVALID; ++it) {
        int i = SmartDigraph::id(g.source(it));
        int j = SmartDigraph::id(g.target(it));
        if (i < j) {
            mip.addRow(y1[i][j] + y2[i][j] + y3[i][j] == 1);
            if (isLog(Debug, log)) {
                std::cout << "  " << wr_y(i,j) << " + " << wr_y2(i,j) << " + " << wr_y3(i,j) << " = 1" << std::endl;
            }
        } else {
            mip.addRow((3 - y1[j][i] - y2[j][i] - y3[j][i]) == 1);
            if (isLog(Debug, log)) {
                std::cout << "  (3 - " << wr_y(j,i) << " - " << wr_y2(j,i) << " - " << wr_y3(j,i) << ") = 1" << std::endl;
            }
        }
    }

    if (isLog(Debug, log)) {
        std::cout << std::endl << "Objective function" << std::endl;
    }
    Mip::Expr mipObj;
    for (int j=0; j<n; ++j) {// j = 1..n
        Node n_j = SmartDigraph::nodeFromId(j);

        for (int k=0; k<j; ++k) {// k = 1..j-1
            Node n_k = SmartDigraph::nodeFromId(k);

            for(OutArcIt kOut(g,n_k); kOut != INVALID; ++kOut) {// végig a k-ból induló éleken
                if (g.runningNode(kOut) == n_j) {// ha (k,j) él
                    if (isLog(Debug, log)) {
                        std::cout << " + " << wr_y(k,j) << " + " << wr_y2(k,j) << " + " << wr_y3(k,j) << std::endl;
                    }

                    mipObj += y1[k][j];// + y_(k,j)
                    mipObj += y2[k][j];// + y'_(k,j)
                    mipObj += y3[k][j];// + y"_(k,j)
                }
            }
        }

        for (int l=j+1; l<n; ++l) {// l = j+1..n
            Node n_l = lemon::SmartDigraph::nodeFromId(l);

            for(OutArcIt lOut(g,n_l); lOut != INVALID; ++lOut) {// végig az l-ből induló éleken
                if (g.runningNode(lOut) == n_j) {// ha (l,j) él
                    if (isLog(Debug, log)) {
                        std::cout << " + (3 - " << wr_y(j,l) << " - " << wr_y2(j,l) << " - " << wr_y3(j,l) << ")"  << std::endl;
                    }

                    mipObj += (3 - y1[j][l] - y2[j][l] - y3[j][l]);
                }
            }
        }
    }

    mip.min();
    mip.obj(mipObj);

    mip.solve();

    if (mip.type() == Mip::OPTIMAL) {
        if (isLog(Info, log)) {
            std::cout << std::endl << "Objective function value: " << mip.solValue() << std::endl;
        }

        if (isLog(Debug, log)) {
            wr_sol(mip, y1, n, &wr_y, true);
            std::cout << std::endl;
            wr_sol(mip, y2, n, &wr_y2, true);
            std::cout << std::endl;
            wr_sol(mip, y3, n, &wr_y3, true);
            std::cout << std::endl;
        }
    } else {
        if (isLog(Info, log)) {
            std::cout << "The optimal solution cannot be found." << std::endl;
        }
        return 1;
    }

    if (isLog(Debug, log)) {
        std::cout << "Creating topological sorting" << std::endl
                  << "  Initializing G'=(V,E') graphs with the node size of the input graph for all y. (|V|=" << n << ")" << std::endl
                  << std::endl;
    }

    SmartDigraph g1;// TODO: 1 temp gráffal működne? másolódik az Arc a végén, v felülírásnál kitörlődik?
    SmartDigraph g2;
    SmartDigraph g3;
    init(g1, n, mip, y1);
    init(g2, n, mip, y2);
    init(g3, n, mip, y3);

    if (!isDAG(log, g1, "G1") || !isDAG(log, g2, "G2") || !isDAG(log, g3, "G3")) {
//        return;// FIXME: ez miért van kikommentelve? upd: hogy ne szakítsa meg sok gráfos futtatást
    }

    SmartDigraph::NodeMap<int> topOrder1(g1);
    topologicalSort(g1, topOrder1);
    SmartDigraph::NodeMap<int> topOrder2(g2);
    topologicalSort(g2, topOrder2);
    SmartDigraph::NodeMap<int> topOrder3(g3);
    topologicalSort(g3, topOrder3);

    if (isLog(Debug, log)) {
        std::cout << std::endl;
        wr_topOrder(topOrder1, n, "G1");
        wr_topOrder(topOrder2, n, "G2");
        wr_topOrder(topOrder3, n, "G3");
        std::cout << std::endl;
    }

    if (isLog(Info, log)) {
        std::cout << "The first found FAS:";
    }
    std::vector<Arc> fas1;
    getArcsForTheFAS(log, g, fas1, topOrder1);

    if (isLog(Info, log)) {
        std::cout << "The second found FAS:";
    }
    std::vector<Arc> fas2;
    getArcsForTheFAS(log, g, fas2, topOrder2);

    if (isLog(Info, log)) {
        std::cout << "The third found FAS:";
    }
    std::vector<Arc> fas3;
    getArcsForTheFAS(log, g, fas3, topOrder3);

    if (isDisjoint(fas1, fas2, fas3)) {
        if (isLog(Debug, log)) {
            std::cout << "  The feedback arc sets are disjoint" << std::endl;
        }
    } else {
        if (isLog(Info, log)) {
            std::cout << std::endl << "Error: the feedback arc sets are not disjoint" << std::endl;
        }
        return 1;
    }

    // TODO: ez az ellenőrzés is kell!
//    if (!isFAS(log, g, fas1) || !isFAS(log, g, fas2) || !isFAS(log, g, fas3)) {
//        if (isLog(Info, log)) {
//            std::cout << std::endl << "Error: at least one of the feedback arc sets are not FAS" << std::endl;
//        }
//        return 1;
//    }
//    if (!isFAS(log, g, fas1)) {
//        if (isLog(Debug, log)) {
//            std::cout << std::endl << "Error: 1 is not FAS" << std::endl;
//        }
//    }
//    if (!isFAS(log, g, fas2)) {
//        if (isLog(Debug, log)) {
//            std::cout << std::endl << "Error: 2 is not FAS" << std::endl;
//        }
//    }
//    if (!isFAS(log, g, fas3)) {
//        if (isLog(Debug, log)) {
//            std::cout << std::endl << "Error: 3 is not FAS" << std::endl;
//        }
//    }

//    return [fas1, fas2, fas3];// TODO: return?
    return 0;
}

void triangleInequalities(const Log &log, Mip &mip, std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int)) {
    for (int i = 0; i < n; ++i) {
        for (int j = i+1; j < n; ++j) {
            for (int k = j+1; k < n; ++k) {
                if (j == i || k == i || k == j) continue;
                Mip::Col y_ij = y[i][j];
                Mip::Col y_ik = y[i][k];
                Mip::Col y_jk = y[j][k];

                if (isLog(Debug, log)) {
                    std::cout << "  " << writer(i,j) << " + " << writer(j,k) << " - " << writer(i,k) << " <= 1" << std::endl;
                }
                mip.addRow(y_ij + y_jk - y_ik <= 1);

                if (isLog(Debug, log)) {
                    std::cout << "  " << writer(i,k) << " - " << writer(i,j) << " - " << writer(j,k) << " <= 0" << std::endl;
                }
                mip.addRow(y_ik - y_ij - y_jk <= 0);
            }
        }
    }

    if (isLog(Debug, log)) {
        std::cout << std::endl;
    }
}

void init(SmartDigraph &g, int nodeSize, const Mip &mip, const std::vector<std::vector<Mip::Col> > &y) {
    g.reserveNode(nodeSize);

    for (int i = 0; i < nodeSize; ++i) {
        g.addNode();
    }

    for (int i = 0; i < nodeSize-1; ++i) {
        for (int j = i+1; j < nodeSize; ++j) {
            if (mip.sol(y[i][j]) == 1) {
                addArc(g, i, j);
            } else if (mip.sol(y[i][j]) == 0) {
                addArc(g, j, i);
            }
        }
    }
}

void init(const SmartDigraph &sourceGraph, SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map) {
    DIGRAPH_TYPEDEFS(SmartDigraph);
    int n = sourceGraph.nodeNum();
    g.reserveNode(n);

    for (int i = 0; i < n; ++i) {
        g.addNode();
    }

    for(ArcIt it(sourceGraph); it != INVALID; ++it) {
        double result = mip.sol(map[it]);
        if (result == 1) {
            Node source = sourceGraph.source(it);
            Node target = sourceGraph.target(it);
            addArc(g, source, target);
        }
    }
}

void addArc(SmartDigraph &g, int sourceNode, int targetNode) {
    SmartDigraph::Node s = SmartDigraph::nodeFromId(sourceNode);
    SmartDigraph::Node t = SmartDigraph::nodeFromId(targetNode);
    addArc(g, s, t);
}

void addArc(SmartDigraph &g, SmartDigraph::Node &source, SmartDigraph::Node &target) {
    g.addArc(source, target);
}

bool isDAG(const Log &log, SmartDigraph &g, const std::string &graphName) {
    bool isDag = dag(g);

    if (isDag) {
        if (isLog(Debug, log)) {
            std::cout << "  The new " << graphName << " graph is a DAG with " << g.arcNum() << " arcs" << std::endl;
        }
    } else {
        if (isLog(Info, log)) {
            std::cout << "Error: " << graphName << " graph is not a DAG!" << std::endl
                      << std::endl;
        }
    }

    return isDag;
}

void getArcsForTheFAS(const Log &log, const SmartDigraph &g, std::vector<SmartDigraph::Arc> &fas,
                      const SmartDigraph::NodeMap<int> &topOrder) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    for(ArcIt it(g); it != INVALID; ++it) {
        Node source = g.source(it);
        Node target = g.target(it);

        if (topOrder[source] < topOrder[target]) {
            if (isLog(Info, log)) {
                std::cout << " (" << SmartDigraph::id(source) + 1 << "," << SmartDigraph::id(target) + 1 << ")";
            }

//            fas.push_back(SmartDigraph::Arc(it));
            fas.push_back(it);
        }
    }

    if (isLog(Info, log)) {
        std::cout << std::endl;
    }
}

bool isDisjoint(const std::vector<SmartDigraph::Arc> &fas1, const std::vector<SmartDigraph::Arc> &fas2,
                const std::vector<SmartDigraph::Arc> &fas3) {
    std::set<SmartDigraph::Arc> arcSet;

    std::for_each(fas1.begin(), fas1.end(), [&](const SmartDigraph::Arc &arc){ arcSet.insert(arc); });
    std::for_each(fas2.begin(), fas2.end(), [&](const SmartDigraph::Arc &arc){ arcSet.insert(arc); });
    std::for_each(fas3.begin(), fas3.end(), [&](const SmartDigraph::Arc &arc){ arcSet.insert(arc); });

    return arcSet.size() == (fas1.size() + fas2.size() + fas3.size());
}

// TODO: szebb megoldás: ki lehet takarni csúcsokat (éleket is?) egy gráfból
bool isFAS(const Log &log, const SmartDigraph &g, const std::vector<SmartDigraph::Arc> &fas) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    ListDigraph d;
    DigraphCopy<SmartDigraph, ListDigraph> dc(g,d);

    SmartDigraph::NodeMap<int> oNodes(g);
    ListDigraph::NodeMap<int> dNodes(d);
    SmartDigraph::ArcMap<int> oArcs(g);
    ListDigraph::ArcMap<int> dArcs(d);
    dc.nodeMap(oNodes, dNodes)
        .arcMap(oArcs, dArcs)
        .run();

    std::for_each(fas.begin(), fas.end(), [&](const SmartDigraph::Arc &arc){
        d.erase(ListDigraph::arcFromId(SmartDigraph::id(arc))); });

    return dag(d);
}


std::string wr_y(int i, int j) {
    return "y_(" + std::to_string(i+1) + "," + std::to_string(j+1) + ")";
}

std::string wr_y2(int i, int j) {
    return "y'_(" + std::to_string(i+1) + "," + std::to_string(j+1) + ")";
}

std::string wr_y3(int i, int j) {
    return "y\"_(" + std::to_string(i+1) + "," + std::to_string(j+1) + ")";
}

void wr_y(const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int)) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            std::cout << "  " << writer(i,j) << "= col[" << Mip::id(y[i][j]) << "]";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void wr_sol(Mip &mip, const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int),
            bool upperTriangMtx) {
    std::string placeholder;
    if (upperTriangMtx) {
        for (int i = 0; i < writer(n,n).size(); ++i) {
            placeholder += "-";
        }
        placeholder += "-----";
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (upperTriangMtx) {
                if (i < j) {
                    std::cout << "  " << writer(i,j) << "= " << mip.sol(y[i][j]);
                } else {
                    std::cout << placeholder;
                }
            } else {
                std::cout << "  " << writer(i,j) << "= " << mip.sol(y[i][j]);
            }
        }
        std::cout << std::endl;
    }
}

void wr_topOrder(const SmartDigraph::NodeMap<int> &topOrder, int n, const std::string &graphName) {
    std::cout << "  Topological order of " << graphName << ":";
    for (int i = 0; i < n; ++i) {
        int id = 0;
        while (i != topOrder[SmartDigraph::nodeFromId(id)] && id < n) {
            ++id;
        }
        std::cout << " " << id + 1;
    }
    std::cout << std::endl;
}

void wr_arcMap(const SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map,
               const std::string &setName) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    std::cout << setName << ":";
    for (ArcIt it(g); it != INVALID; ++it) {
        double result = mip.sol(map[it]);
        if (result == 1) {
            std::cout << " " << SmartDigraph::id(it);
        }
    }
    std::cout << std::endl;
}
