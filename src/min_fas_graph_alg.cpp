
#include <lemon/smart_graph.h>
#include <lemon/lgf_reader.h>
#include "common/console_logger.h"
#include "common/state_tree.h"

using namespace lemon;

//typedef std::vector<SmartDigraph::Node> NodeList;
//typedef std::vector<SmartDigraph::Node *> NodePtrList;

int minFAS(const SmartDigraph &g, const SmartDigraph::ArcMap<int> &arcMap, const Log &log = NoLog);

int minFAS(const NodePtrList &nodes, const WeightMap &weights, int opt, const Log &log = NoLog);

int start_opt(const SmartDigraph &g, const Log &log);

std::vector<SmartDigraph::Node> get_nodes(const SmartDigraph &g, const Log &log);

std::vector<SmartDigraph::Node> copyNodes(const SmartDigraph &g, const Log &log);

std::vector<const SmartDigraph::Node *> createNodePointerList(const std::vector<SmartDigraph::Node> &source,
                                                              const Log &log);


std::vector<const SmartDigraph::Node *> nodes_filter(const SmartDigraph::Node &,
                                                     const std::vector<SmartDigraph::Node> &, const Log &);

std::vector<SmartDigraph::Node *> create_tree(const SmartDigraph::Node &source,
                                              std::vector<SmartDigraph::Node *> &nodes, int &curr_opt, const Log &log);

int start_tree(const SmartDigraph &g, const Log &log);

// INFO: run: ./min-fas <path_of_base_dir> <lgf_file_name_with_extension> <optional_arg: -l (info || debug)>
int main(int argc, const char *argv[]) {
    Log log = getLog(argc, argv);

//    DIGRAPH_TYPEDEFS(SmartDigraph);

    if (argc < 3) {
        return -1;
    }

    std::string path(argv[1]);
    std::string graphFile(argv[2]);

    SmartDigraph g;
    SmartDigraph::ArcMap<int> weights(g);
    try {
        digraphReader(g, path + "/" + graphFile)
                .arcMap("weight", weights)
                .run();
    } catch (Exception &error) {
        if (isInfo(log)) {
            std::cerr << "Error: " << error.what() << std::endl;
        }
        return -1;
    }

    int result = minFAS(g, weights, log);
    std::cout << std::endl << "Result: " << result << std::endl;

    return 0;
}

int minFAS(const SmartDigraph &g, const SmartDigraph::ArcMap<int> &arcMap, const Log &log) {
    std::vector<SmartDigraph::Node> nodeRefList = copyNodes(g, log);
    NodePtrList nodes = createNodePointerList(nodeRefList, log);

    WeightMap weights(g, arcMap);
    if (isDebug(log)) std::cout << std::endl << weights.toString() << std::endl << std::endl;

    int opt = start_opt(g, log);

    return minFAS(nodes, weights, opt, log);
}

int minFAS(const NodePtrList &nodes, const WeightMap &weights, int opt, const Log &log) {
    int result = opt;
    if (isInfo(log)) std::cout << "Starting optimum: " << result << std::endl;

    for (auto node = nodes.begin(); node != nodes.end(); ++node) {
        const SmartDigraph::Node* root(*node);// INFO: másolom, h const legyen

        if (isInfo(log)) std::cout << std::endl << "Creating tree with root: " << getId(root) << std::endl;
        StateTree tree(root, nodes, result, log);// INFO: a nodes-t a StateTreeBase.setNodes() másolja

        tree.process(weights);
        if (tree.getOptWeight() < result) {
            result = tree.getOptWeight();
        }

        if (isDebug(log)) {
            break;// TODO: run with the first node only
        }
    }

    return result;
}


int start_opt(const SmartDigraph &g, const Log &log) {
    return 11;// TODO: WIP
}

std::vector<SmartDigraph::Node> copyNodes(const SmartDigraph &g, const Log &log) {
    std::vector<SmartDigraph::Node> nodes;
    nodes.reserve(g.nodeNum());

    std::for_each(g.nodes().begin(), g.nodes().end(), [&](const SmartDigraph::Node &node) {
        nodes.push_back(node);
    });

    if (isInfo(log)) {
        std::reverse(nodes.begin(), nodes.end());// FIXME: tudom, h drága és felesleges...
        std::cout << std::endl << "copyNodes(): node list created with size: " << nodes.size() << std::endl;

        std::for_each(nodes.begin(), nodes.end(), [&](const SmartDigraph::Node &node) {
            std::cout << " " << SmartDigraph::id(node) + 1;
        });
        std::cout << std::endl;
    }

    return nodes;
}

NodePtrList createNodePointerList(const std::vector<SmartDigraph::Node> &source, const Log &log) {
    NodePtrList nodes;
    nodes.reserve(source.size());

    std::for_each(source.begin(), source.end(), [&](const SmartDigraph::Node &node) {
        nodes.push_back(&node);
    });

    if (isInfo(log)) {
        std::cout << std::endl << "createNodePointerList(): node pointer list created with size: " << nodes.size() << std::endl;
        std::for_each(nodes.begin(), nodes.end(), [&](const SmartDigraph::Node *&node) {
            std::cout << " " << SmartDigraph::id(*node) + 1;
        });
        std::cout << std::endl;
    }

    return nodes;
}

// csak kíváncsiság (const Node és const ptr fogja):
//std::vector<const SmartDigraph::Node* const> test(const SmartDigraph &g) {
//    std::vector<const SmartDigraph::Node* const> nodes;
//    nodes.reserve(g.nodeNum());// ez nem megy const-ként (meg a fordítás se valszeg)
//
//    std::for_each(g.nodes().begin(), g.nodes().end(), [&](const SmartDigraph::Node &node) {
//        const SmartDigraph::Node *const p = &node;
//        nodes.push_back(p);
//    });
//
//    return nodes;
//}


std::vector<SmartDigraph::Node> get_nodes(const SmartDigraph &g, const Log &log) {
    std::vector<SmartDigraph::Node> nodes;
    nodes.reserve(g.nodeNum());

    std::for_each(g.nodes().begin(), g.nodes().end(), [&](const SmartDigraph::Node &node) { nodes.push_back(node); });

    if (isDebug(log)) {
        std::reverse(nodes.begin(), nodes.end());// FIXME: tudom, h drága és felesleges...

        std::cout << "get_nodes: add nodes: " << std::endl;
        std::for_each(nodes.begin(), nodes.end(), [&](const SmartDigraph::Node &node) {
            std::cout << " " << SmartDigraph::id(node) + 1;
        });
        std::cout << std::endl;
    }

    if (isInfo(log)) {
        std::cout << "Node list created with size: " << nodes.size() << std::endl;
    }

    return nodes;
}

std::vector<const SmartDigraph::Node *> nodes_filter(const SmartDigraph::Node &filter,
                                                     const std::vector<SmartDigraph::Node> &source, const Log &log) {
    std::vector<const SmartDigraph::Node *> filteredNodes;
    filteredNodes.reserve(source.size() - 1);

    std::for_each(source.begin(), source.end(), [&](const SmartDigraph::Node &node) {
        if (node != filter) {
            filteredNodes.push_back(&node);
        }
    });

    if (isDebug(log)) {
        std::cout << "nodes_filter: add pointers of nodes: " << std::endl;
        std::for_each(filteredNodes.begin(), filteredNodes.end(), [&](const SmartDigraph::Node *&node) {
            std::cout << " " << SmartDigraph::id(*node) + 1;
        });
        std::cout << std::endl;
    }

//    for (int i=0; i<source.size(); ++i) {
//        if (source[i] != filter) {
//            filteredNodes.push_back(&source[i]);
//        }
//    }
//    for (auto it = source.begin(); it != source.end(); ++it) {
//        filteredNodes.push_back(&it);
//    }

    if (isInfo(log)) {
        std::cout << "Filtered node pointer list created with size: " << filteredNodes.size() << std::endl;
    }

    return filteredNodes;
}

std::vector<SmartDigraph::Node *>
create_tree(const SmartDigraph::Node &source, std::vector<SmartDigraph::Node *> &nodes,
            int &curr_opt, const Log &log) {
    if (isDebug(log)) {
        std::cout << "Start create_tree with root [" << SmartDigraph::id(source) << "] and current opt ["
                  << curr_opt << "]" << std::endl;
    }

    return nodes;
}



