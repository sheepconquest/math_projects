#include <iostream>
#include <lemon/smart_graph.h>
#include <lemon/concepts/digraph.h>
#include <lemon/lgf_reader.h>
#include <lemon/lgf_writer.h>
#include <lemon/dijkstra.h>

using namespace lemon;

int main() {
    std::cout << "Hello, World!" << std::endl;

    SmartDigraph g;
    SmartDigraph::ArcMap<int> cap(g);
//    concepts::ReadWriteMap<concepts::Digraph::Arc,int> lengthMap;
//    concepts::ReadWriteMap<SmartDigraph::Arc,int> lengthMap{};
    SmartDigraph::Node s, t;
//    ListDigraph g;
//    ListDigraph::ArcMap<int> cap(g);
//    ListDigraph::Node s, t;

    try {
        digraphReader(g, "data/digraph_map.lgf")     // read the directed graph into g
            .arcMap("capacity", cap)        // read the 'capacity' arc map into cap
            .node("source", s)                         // read 'source' node to s
            .node("target", t)              // read 'target' node to t
            .run();
    } catch (Exception &error) {                       // check if there was any error
        std::cerr << "Error: " << error.what() << std::endl;
        return -1;
    }

    std::cout << "A digraph is read from 'digraph_map.lgf'." << std::endl;
    std::cout << "Number of nodes: " << countNodes(g) << std::endl;
    std::cout << "Number of arcs: " << countArcs(g) << std::endl;

    std::cout << "We can write it to the standard output:" << std::endl;

    digraphWriter(g)                            // write g to the standard output
            .arcMap("capacity", cap)    // write cap into 'capacity'
            .node("source", s)                  // write s to 'source'
            .node("target", t)          // write t to 'target'
            .run();

    std::cout << "Dijkstra algorithm." << std::endl;

    // TESTS
//    checkDijkstra<ListDigraph>();
//    checkDijkstra<SmartDigraph>();

    Dijkstra<SmartDigraph, SmartDigraph::ArcMap<int> > dijkstra(g,cap);
    dijkstra.init();

    bool reached = dijkstra.run(s,t);

    if (reached) {
        std::cout << "There is a path between s and t with distance: " << dijkstra.dist(t) << std::endl;
    } else {
        std::cout << "There is no path between s and t." << std::endl;
    }

    return 0;
}
