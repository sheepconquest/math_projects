#include <lemon/lp.h>
#include <lemon/smart_graph.h>
#include <lemon/lgf_reader.h>
#include <lemon/connectivity.h>
#include <set>
#include "common/console_logger.h"

using namespace lemon;

void lpDemo();
void lpFas(const SmartDigraph &g);
//void mipWithLemon(const SmartDigraph &g);

void singleMinFeedbackArcSet(const SmartDigraph &g, const Log &log = Info);

// TODO: WIP: nem készült el az ellenőrzés
//template <typename GR>
//void controlFas(const GR &g);
void controlFas(const SmartDigraph &g, const Log &log = Info);

// INFO: ezek mehetnek egy FAS common-ba
void triangleInequalities(const Log &log, Mip &mip, std::vector<std::vector<Mip::Col> > &y, int n,
                          std::string (*writer) (int, int));

void init(SmartDigraph &g, int nodeSize, const Mip &mip, const std::vector<std::vector<Mip::Col> > &y);
void init(const SmartDigraph &sourceGraph, SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map);
void addArc(SmartDigraph &g, int sourceNode, int targetNode);
void addArc(SmartDigraph &g, SmartDigraph::Node &source, SmartDigraph::Node &target);
bool isDAG(const Log &log, SmartDigraph &g, const std::string &graphName = "G'");
void getArcsForTheFAS(const Log &log, const SmartDigraph &g, std::vector<SmartDigraph::Arc> &fas,
                      const SmartDigraph::NodeMap<int> &topOrder);

// INFO: ezek mehetnek egy FAS common-ba v. a LOG-hoz
std::string wr_y(int i, int j);
std::string wr_y2(int i, int j);
std::string wr_y3(int i, int j);
void wr_y(const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int));
void wr_sol(Mip &mip, const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int),
            bool upperTriangMtx = false);
void wr_topOrder(const SmartDigraph::NodeMap<int> &topOrder, int n, const std::string &graphName = "G'");
void wr_arcMap(const SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map,
               const std::string &setName);

int main(int argc, const char* argv[]) {
    Log log = getLog(argc, argv);

    DIGRAPH_TYPEDEFS(SmartDigraph);

//    lpDemo();

    SmartDigraph g;
//    SmartDigraph::ArcMap<int> cap(g);

    try {
        digraphReader(g, "data/digraph_map_fas2.lgf")
//        digraphReader(g, "data/diPlanarGraph_4.lgf")
                .run();
    } catch (Exception &error) {
        std::cerr << "Error: " << error.what() << std::endl;
        return -1;
    }

//    std::cout << "Number of nodes: " << countNodes(g) << std::endl;
//    std::cout << "Number of arcs: " << countArcs(g) << std::endl;
    int n = g.nodeNum();
    int v = g.arcNum();
    std::cout << "Number of nodes: " << n << std::endl;
    std::cout << "Number of arcs: " << v << std::endl;

//    mipWithLemon(g);// TODO: ArrayMap módosítást igényelt

    singleMinFeedbackArcSet(g, log);

//    controlFas(g, Debug);

    return 0;
}

void lpDemo() {
    // Create an instance of the default LP solver class
    // (it will represent an "empty" problem at first)
    Lp lp;

    // Add two columns (variables) to the problem
    Lp::Col x1 = lp.addCol();
    Lp::Col x2 = lp.addCol();

    // Add rows (constraints) to the problem
    lp.addRow(x1 - 5 <= x2);
    lp.addRow(0 <= 2 * x1 + x2 <= 25);

    // Set lower and upper bounds for the columns (variables)
    lp.colLowerBound(x1, 0);
    lp.colUpperBound(x2, 10);

    // Specify the objective function
    lp.max();
    lp.obj(5 * x1 + 3 * x2);

    // Solve the problem using the underlying LP solver
    lp.solve();

    // Print the results
    if (lp.primalType() == Lp::OPTIMAL) {
        std::cout << "Objective function value: " << lp.primal() << std::endl;
        std::cout << "x1 = " << lp.primal(x1) << std::endl;
        std::cout << "x2 = " << lp.primal(x2) << std::endl;
    } else {
        std::cout << "Optimal solution not found." << std::endl;
    }
}

// TODO: átnézni, lehet hibás eredményt ad
void lpFas(const SmartDigraph &g) {
    std::cout << std::endl << std::endl << "--- LP solving with CPLEX ---" << std::endl;

    DIGRAPH_TYPEDEFS(SmartDigraph);

    Lp fasLp;

    // Add a column to the problem for each arc
    SmartDigraph::ArcMap<Lp::Col> y(g);
    fasLp.addColSet(y);

    // Constraint
    for (ArcIt it(g); it != INVALID; ++it) {
        fasLp.colLowerBound(y[it], 0);
        fasLp.colUpperBound(y[it], 1);
    }
//    for (Lp::ColIt col(fasLp); col != INVALID; ++col) {
//        fasLp.colLowerBound(col, 0);
//    }

    // Constraint: triangle inequalities
    // kell: ha i->j él és j->k él és i->k él => y[i] + y[j] - y[k] <= 1 és -y[i] - y[j] + y[k] <= 0
//    for (NodeIt i(g); i != INVALID; ++i) {// végig az összes csúcsból kiindulva (i)
//        for (OutArcIt ij(g, i); ij != INVALID; ++ij) {// i->j él
//            Node j = g.runningNode(ij);
//            for (OutArcIt jk(g, j); jk != INVALID; ++jk) {// j->k él
//
//            }
//        }
//    }
    // vagy minden y-on?
//    for (Lp::ColIt i(fasLp); i != INVALID; ++i) {
//        for (Lp::ColIt j(fasLp); j != INVALID; ++j) {
//            if (j == i) continue;
//            for (Lp::ColIt k(fasLp); k != INVALID; ++k) {
//                if (k == i || k == j) continue;
//
//
//            }
//        }
//    }
    for (ArcIt i(g); i != INVALID; ++i) {
        for (ArcIt j(g); j != INVALID; ++j) {
            if (j == i) continue;
            for (ArcIt k(g); k != INVALID; ++k) {
                if (k == i || k == j) continue;
                Lp::Expr e1;
                e1 += y[i];
                e1 += y[j];
                e1 -= y[k];
                fasLp.addRow(e1 <= 1);
                Lp::Expr e2;
                e2 -= y[i];
                e2 -= y[j];
                e2 += y[k];
                fasLp.addRow(e2 <= 0);
            }
        }
    }

    // Objective function
    Lp::Expr objective;
    for (NodeIt node(g); node != INVALID; ++node) {
        for (NodeIt k(g); k != node; ++k) {
            for(OutArcIt kOut(g,k); kOut != INVALID; ++kOut) {
                if (g.runningNode(kOut) == node) {
                    objective += y[kOut];
                }
            }
        }
        for (NodeIt l(g,node); l != INVALID; ++l) {
            for(OutArcIt lOut(g,l); lOut != INVALID; ++lOut) {
                if (g.runningNode(lOut) == node) {
                    objective += (1 - y[lOut]);
                }
            }
        }
//        for (InArcIt inArc(g, node); inArc != INVALID; ++inArc) {
//            objective += y[inArc];
//        }
    }
    fasLp.min();
    fasLp.obj(objective);// direkt kell a min után?

    fasLp.solve();

    if (fasLp.primalType() == Lp::OPTIMAL) {
        std::cout << "Objective function value: " << fasLp.primal() << std::endl;

        for (Lp::ColIt col(fasLp); col != INVALID; ++col) {
            int colId = Lp::id(col);
            std::cout << "y" << colId << "= " << fasLp.primal(Lp::colFromId(colId)) << std::endl;
        }
    } else {
        std::cout << "Optimal solution not found." << std::endl;
    }
}

/*
void mipWithLemon(const SmartDigraph &g) {// TODO: ArrayMap módosítást igényelt
    DIGRAPH_TYPEDEFS(SmartDigraph);

    std::cout << std::endl << std::endl << "--- MIP solving with CPLEX ---" << std::endl;

    Mip mip;// nem ez kell? itt lehet int col változót definiálni és akkor y>=0 és y<=1
    // Add a column to the problem for each arc
//    SmartDigraph::ArcMap<Mip::Col> x(g);
//    mip.addColSet(x);


//    ArrayMap<SmartDigraph, SmartDigraph::Node, SmartDigraph::NodeMap<Mip::Col> > y_h(g);
//    concepts::ReadWriteMap<SmartDigraph::NodeIt, SmartDigraph::NodeMap<Mip::Col> > y_h;
//    SmartDigraph::NodeMap<SmartDigraph::NodeMap<Mip::Col> > y_h();

    struct NodeHasher {
        size_t operator()(const SmartDigraph::Node& n) const {
            return (SmartDigraph::id(n) + 11) * 13;
        }
    };

    std::unordered_map<SmartDigraph::Node, SmartDigraph::NodeMap<Mip::Col>, NodeHasher> y_v;
    int n = g.nodeNum();
    y_v.reserve(n);
//    std::map<SmartDigraph::NodeIt, SmartDigraph::NodeMap<Mip::Col> > y_v;

//    SmartDigraph::NodeMap<Mip::Col> y_h(g);
    for (SmartDigraph::NodeIt it(g); it != INVALID; ++it) {
        Node node = static_cast<Node>(it);
        SmartDigraph::NodeMap<Mip::Col> map(g);

//        y_v.insert(std::pair<SmartDigraph::Node, SmartDigraph::NodeMap<Mip::Col> >(node, y_h));
        std::cout << "insert map to node: " << SmartDigraph::id(node)+1 << std::endl;
        y_v.insert(std::pair<SmartDigraph::Node, SmartDigraph::NodeMap<Mip::Col> >(node, map));//TODO: ArrayMap módosítás

        mip.addColSet(y_v.find(node)->second);//TODO: ArrayMap módosítás
//        mip.addColSet(y_v.at(node));//ArrayMap módosítás nélkül próba
    }

//    std::unordered_map<SmartDigraph::Node, SmartDigraph::NodeMap<Mip::Col>, NodeHasher>::iterator itr;
    std::cout << "All Elements:" << std::endl;
    int row = 1, column = 1;
//    for (itr = y_v.begin(); itr != y_v.end(); ++itr) {
    for (NodeIt itr(g); itr != INVALID; ++itr) {
        // itr works as a pointer to pair<string, double>
        // type itr->first stores the key part  and
        // itr->second stores the value part
//        std::cout << itr->first << "  " << itr->second << std::endl;

        column = 1;
        std::cout << "row " << row << std::endl;

//        Node n1 = itr->first;
        Node n1 = (Node) itr;
//        SmartDigraph::NodeMap<Mip::Col> nodeMap = itr->second;

        SmartDigraph::NodeMap<Mip::Col> nodeMap = y_v.find(n1)->second;//TODO: ArrayMap módosítás
//        SmartDigraph::NodeMap<Mip::Col> &nodeMap = y_v.at(n1);//ArrayMap módosítás nélkül próba
//        SmartDigraph::NodeMap<Mip::Col> &nodeMap = y_v[n1];//ArrayMap módosítás nélkül

//        NodeIt row_it;// nincs értelme
        for (NodeIt it(g); it != INVALID; ++it) {
//        for (row_it = g.nodes().begin(); row_it != g.nodes().end(); ++row_it) {

//            Mip::Col col = mip.addCol();// nem tudom egyesével hozzáadni
//            mip.colUpperBound(col, 1);

            // Constraint
            Mip::Col col = nodeMap[it];
//            Mip::Col col = nodeMap[row_it];
            mip.colLowerBound(col, 0);
            mip.colUpperBound(col, 1);
            mip.colType(col, Mip::INTEGER);

            Node n2 = (Node) it;
//            Node n2 = (Node) row_it;
            std::cout << " y_(" << SmartDigraph::id(n1)+1 << "," << SmartDigraph::id(n2)+1 << ") col id:" << Mip::id(col) << ", ";
        }

        std::cout << std::endl;

        row++;
    }

    // Constraint
//    for (ArcIt it(g); it != INVALID; ++it) {
//        mip.colLowerBound(x[it], 0);
//        mip.colUpperBound(x[it], 1);
//        mip.colType(x[it], Mip::INTEGER);
//    }

    // Constraint: triangle inequalities
//    for (ArcIt i(g); i != INVALID; ++i) {
//        for (ArcIt j(g); j != INVALID; ++j) {
//            if (j == i) continue;
//            for (ArcIt k(g); k != INVALID; ++k) {
//                if (k == i || k == j) continue;
//                Mip::Expr e1;
//                e1 += x[i];
//                e1 += x[j];
//                e1 -= x[k];
//                mip.addRow(e1 <= 1);
//                Mip::Expr e2;
//                e2 -= x[i];
//                e2 -= x[j];
//                e2 += x[k];
//                mip.addRow(e2 <= 0);
//            }
//        }
//    }
    std::cout << std::endl << "Constraint: triangle inequalities" << std::endl;
    for (NodeIt i(g); i != INVALID; ++i) {// i = 1 .. n
        for (NodeIt j(g, i); j != INVALID; ++j) {// j = i .. n
//            if (j == i) continue;
            for (NodeIt k(g, j); k != INVALID; ++k) {// k = j .. n
                if (j == i || k == i || k == j) continue;
//                if (k == i || k == j) continue;
                Mip::Col y_ij = y_v.find(i)->second[j];
                Mip::Col y_ik = y_v.find(i)->second[k];
                Mip::Col y_jk = y_v.find(j)->second[k];
                std::cout << "col(" << Mip::id(y_ij) << ") + col(" << Mip::id(y_jk) << ") - col(" << Mip::id(y_ik) << ") <= 1" << std::endl;
                mip.addRow(y_ij + y_jk - y_ik <= 1);
                std::cout << "col(" << Mip::id(y_ik) << ") - col(" << Mip::id(y_ij) << ") - col(" << Mip::id(y_jk) << ") <= 0" << std::endl;
                mip.addRow(y_ik - y_ij - y_jk <= 0);
            }
        }
    }

    // Objective function
//    Mip::Expr mipObj;
//    for (NodeIt node(g); node != INVALID; ++node) {
//        for (NodeIt k(g); k != node; ++k) {
//            for(OutArcIt kOut(g,k); kOut != INVALID; ++kOut) {
//                if (g.runningNode(kOut) == node) {
//                    mipObj += x[kOut];
//                }
//            }
//        }
//        for (NodeIt l(g,node); l != INVALID; ++l) {
//            for(OutArcIt lOut(g,l); lOut != INVALID; ++lOut) {
//                if (g.runningNode(lOut) == node) {
//                    mipObj += (1 - x[lOut]);
//                }
//            }
//        }
//    }
    std::cout << std::endl << "Objective function" << std::endl;
    Mip::Expr mipObj;
    for (NodeIt j(g); j != INVALID; ++j) {// j = 1..n
        for (NodeIt k(g); k != j; ++k) {// k = 1..j
            for(OutArcIt kOut(g,k); kOut != INVALID; ++kOut) {// végig a k-ból induló éleken
                if (g.runningNode(kOut) == j) {// ha (k,j) él
                    Mip::Col y_kj = y_v.find(k)->second[j];
                    std::cout << "+ y_(" << SmartDigraph::id(k)+1 << "," << SmartDigraph::id(j)+1 << ") ";
                    mipObj += y_kj;// + y_(k,j)
                }
            }
        }
        for (NodeIt l(g,j); l != INVALID; ++l) {// l = j..n
            for(OutArcIt lOut(g,l); lOut != INVALID; ++lOut) {// végig az l-ből induló éleken
                if (g.runningNode(lOut) == j) {// ha (l,j) él
                    std::cout << "+ (1 - y_(" << SmartDigraph::id(j)+1 << "," << SmartDigraph::id(l)+1 << ")) ";
                    mipObj += (1 - y_v.find(j)->second[l]);// + (1 - y_(j,l))
                }
            }
        }
    }
    std::cout << std::endl;

    mip.min();
    mip.obj(mipObj);

    mip.solve();

    std::cout << std::endl;
    if (mip.type() == Mip::OPTIMAL) {
        std::cout << "Objective function value: " << mip.solValue() << std::endl;

//        for (Mip::ColIt col(mip); col != INVALID; ++col) {
//            int colId = Mip::id(col);
////            std::cout << "x" << colId << "= " << mip.sol(Mip::colFromId(colId)) << std::endl;
//            std::cout << "col(" << colId << ")= " << mip.sol((Mip::Col) col) << std::endl;
//        }

        std::unordered_map<SmartDigraph::Node, SmartDigraph::NodeMap<Mip::Col>, NodeHasher>::iterator itr;
        for (itr = y_v.begin(); itr != y_v.end(); ++itr) {
            Node n1 = itr->first;
            SmartDigraph::NodeMap<Mip::Col> nodeMap = itr->second;//TODO: ArrayMap módosítás
//            SmartDigraph::NodeMap<Mip::Col> &nodeMap = itr->second;//ArrayMap módosítás nélkül próba
            for (NodeIt it(g); it != INVALID; ++it) {
                Mip::Col col = nodeMap[it];
                Node n2 = (Node) it;
                std::cout << "y_(" << SmartDigraph::id(n1)+1 << "," << SmartDigraph::id(n2)+1 << ")= " << mip.sol(col) << "  ";
            }
            std::cout << std::endl;
        }
    } else {
        std::cout << "Optimal solution not found." << std::endl;
    }
}
*/

void singleMinFeedbackArcSet(const SmartDigraph &g, const Log &log) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    if (isLog(Debug, log)) {
        std::cout << std::endl << "--- Finding a minimum feedback arc set by MIP solving with CPLEX (Node id based implementation) ---"
                  << std::endl << std::endl;
    }

    if (dag(g)) {
        if (isLog(Info, log)) {
            std::cout << "The graph is a DAG, no feedback arc set can be found" << std::endl;
        }
        return;
    }

    Mip mip;
    std::vector<std::vector<Mip::Col> > y;

    int n = g.nodeNum();

    if (isLog(Debug, log)) {
        std::cout << "Initializing Mip::Col elements and adding to y matrix" << std::endl;
    }
    for (int i = 0; i < n; ++i) {
        std::vector<Mip::Col> row;

        for (int j = 0; j < n; ++j) {
//            Mip::Col col;// ha nem akarom hozzáadni az i=j esetén a Mip::Col-t

            Mip::Col col = mip.addCol();
            mip.colLowerBound(col, 0);
            mip.colUpperBound(col, 1);
            mip.colType(col, Mip::INTEGER);

            row.push_back(col);

            if (isLog(Debug, log)) {
                std::cout << "  " << wr_y(i,j) << "= col[" << Mip::id(col) << "]";
            }
        }

        y.push_back(row);

        if (isLog(Debug, log)) {
            std::cout << std::endl;
        }
    }

    // ha nem akarom hozzáadni az i=j esetén a Mip::Col-t
//    for (int i = 0; i < n; ++i) {
//        std::vector<Mip::Col> row;
//
//        int count = 0;
//        for (int j = 0; j < n; ++j) {
//            if (i == j) continue;
//            count++;
//            Mip::Col col = y[i][j];
//            mip.colLowerBound(col, 0);
//            mip.colUpperBound(col, 1);
//            mip.colType(col, Mip::INTEGER);
//            row.push_back(col);
//        }
//        std::cout << "insert " << count << " element to mip" << std::endl;
//        mip.addColSet(row);
//    }

//    for (Mip::ColIt col(mip); col != INVALID; ++col) {
//        int colId = Mip::id(col);
//        std::cout << "col(" << colId << ")" << std::endl;
//    }

    if (isLog(Debug, log)) {
        std::cout << std::endl << "Constraint: triangle inequalities" << std::endl;
    }
    triangleInequalities(log, mip, y, n, &wr_y);

    if (isLog(Debug, log)) {
        std::cout << "Objective function" << std::endl;
    }
    Mip::Expr mipObj;
    for (int j=0; j<n; ++j) {// j = 1..n
        Node n_j = lemon::SmartDigraph::nodeFromId(j);

        for (int k=0; k<j; ++k) {// k = 1..j-1
            Node n_k = lemon::SmartDigraph::nodeFromId(k);

            for(OutArcIt kOut(g,n_k); kOut != INVALID; ++kOut) {// végig a k-ból induló éleken
                if (g.runningNode(kOut) == n_j) {// ha (k,j) él
                    if (isLog(Debug, log)) {
                        std::cout << " + " << wr_y(k,j);
                    }

                    mipObj += y[k][j];// + y_(k,j)
                }
            }
        }

        for (int l=j+1; l<n; ++l) {// l = j+1..n
            Node n_l = lemon::SmartDigraph::nodeFromId(l);

            for(OutArcIt lOut(g,n_l); lOut != INVALID; ++lOut) {// végig az l-ből induló éleken
                if (g.runningNode(lOut) == n_j) {// ha (l,j) él
                    if (isLog(Debug, log)) {
                        std::cout << " + (1 - " << wr_y(j,l) << ")";
                    }

                    mipObj += (1 - y[j][l]);// + (1 - y_(j,l))
                }
            }
        }
    }

    if (isLog(Debug, log)) {
        std::cout << std::endl;
    }

    mip.min();
    mip.obj(mipObj);

    mip.solve();

    if (mip.type() == Mip::OPTIMAL) {
        if (isLog(Info, log)) {
            std::cout << std::endl << "Objective function value: " << mip.solValue() << std::endl;
        }

        if (isLog(Debug, log)) {
            wr_sol(mip, y, n, &wr_y, true);
        }
    } else {
        if (isLog(Info, log)) {
            std::cout << "The optimal solution cannot be found." << std::endl;
        }
        return;
    }

    if (isLog(Debug, log)) {
        std::cout << std::endl << "Creating topological sorting" << std::endl
                  << "  Initializing G'=(V,E') graph with the node size of the input graph. (|V|=" << n << ")" << std::endl;
    }

    SmartDigraph g2;
    init(g2, n, mip, y);

    if (!isDAG(log, g2)) {
        return;
    }

    SmartDigraph::NodeMap<int> topOrder(g2);
    topologicalSort(g2, topOrder);

    if (isLog(Debug, log)) {
        wr_topOrder(topOrder, n);
        std::cout << std::endl;
    }

    if (isLog(Info, log)) {
        std::cout << "The found FAS:";
    }
    std::vector<Arc> fas;
    getArcsForTheFAS(log, g, fas, topOrder);

//    return fas;// TODO: return?
}


//template <typename GR>// template
//void controlFas(const GR &g) {// template
void controlFas(const SmartDigraph &g, const Log &log) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    if (isLog(Debug, log)) {
        std::cout << std::endl << "--- Finding 3 disjoint feedback arc sets (control) ---" << std::endl << std::endl;
    }

    if (dag(g)) {
        if (isLog(Info, log)) {
            std::cout << "The graph is a DAG, no feedback arc set can be found" << std::endl;
        }
        return;
    }

    Mip mip;
//    typename GR::template ArcMap<int> arcs(g);// template

    int nodeSize = g.nodeNum();

//    SmartDigraph g2;
//    g2.addNode();

    // v1
//    int n = g.arcNum();
//    int e = g.arcNum();
//    for (int i = 0; i < e; ++i) {
//        // 3 x ArcMap
//        Mip::Col x1 = mip.addCol();// 0 + k*3 - cycle1
//        Mip::Col x2 = mip.addCol();// 1 + k*3 - cycle2
//        Mip::Col x3 = mip.addCol();// 2 + k*3 - cycle3
//
//        mip.colType(x1, Mip::INTEGER);
//        mip.colType(x2, Mip::INTEGER);
//        mip.colType(x3, Mip::INTEGER);
//
//        mip.colBounds(x1, 0, 1);
//        mip.colBounds(x2, 0, 1);
//        mip.colBounds(x3, 0, 1);
//
//        Mip::Expr expr;
//        expr += x1;
//        expr += x2;
//        expr += x3;
//        mip.addRow(expr == 1);
//    }

    // v2 - ArcMap
    SmartDigraph::ArcMap<Mip::Col> fas1(g);
    SmartDigraph::ArcMap<Mip::Col> fas2(g);
    SmartDigraph::ArcMap<Mip::Col> fas3(g);
    mip.addColSet(fas1);
    mip.addColSet(fas2);
    mip.addColSet(fas3);

    if (isLog(Debug, log)) {
        std::cout << "Constraint: disjoint sets" << std::endl;
    }
    for (ArcIt it(g); it != INVALID; ++it) {
        mip.colType(fas1[it], Mip::INTEGER);
        mip.colBounds(fas1[it], 0, 1);
        mip.colType(fas2[it], Mip::INTEGER);
        mip.colBounds(fas2[it], 0, 1);
        mip.colType(fas3[it], Mip::INTEGER);
        mip.colBounds(fas3[it], 0, 1);

        mip.addRow(fas1[it] + fas2[it] + fas3[it] == 1);
        if (isLog(Debug, log)) {
            std::cout << "  col[" << Mip::id(fas1[it]) << "] + col[" << Mip::id(fas2[it]) << "] + col["
                      << Mip::id(fas3[it]) << "] = 1" << std::endl;
        }
    }

//    mip.obj()// TODO: ez mi lesz - nem kell igazából?
    Mip::Expr obj;
    for (Mip::ColIt col(mip); col != INVALID; ++col) {
        obj += col;
    }
    mip.max();
    mip.obj(obj);

    mip.solve();

    if (mip.type() == Mip::OPTIMAL) {
        if (isLog(Info, log)) {
            std::cout << "Objective function value: " << mip.solValue() << std::endl;
        }

        // v1
//        for (Mip::ColIt col(mip); col != INVALID; ++col) {
//            int colId = Mip::id(col);
//            std::cout << "x" << colId << "= " << mip.sol(Mip::colFromId(colId)) << std::endl;
//        }

        // v2
        if (isLog(Debug, log)) {
            wr_arcMap(g, mip, fas1, "ArcSet 1");
            wr_arcMap(g, mip, fas2, "ArcSet 2");
            wr_arcMap(g, mip, fas3, "ArcSet 3");
        }
    } else {
        if (isLog(Info, log)) {
            std::cout << "Optimal solution not found." << std::endl;
        }
    }

//    typename GR::template::SmartDigraph g2;// template
    SmartDigraph g2;

    // v1
//    for (int i = 0; i < n; ++i) {
//        g2.addNode();
//    }

    // kör1
    // v1
//    for (int i = 0; i < e; ++i) {
//        int id = 0 + 3*i;
//        if (mip.sol(Mip::colFromId(id)) == 1) {
//            SmartDigraph::Arc arc = SmartDigraph::arcFromId(i);
//            SmartDigraph::Node source = g.source(arc);
//            SmartDigraph::Node target = g.target(arc);
//
//            g2.addArc(SmartDigraph::nodeFromId(SmartDigraph::id(source)),
//                      SmartDigraph::nodeFromId(SmartDigraph::id(target)));
//        }
//    }
    // v2
    init(g, g2, mip, fas1);

    if (isLog(Debug, log)) {
        std::cout << "G2:" << std::endl;
        digraphWriter(g2).run();
    }

//    LEMON_CONNECTIVITY_H
    if (dag(g2)) {
//    if (dag2(g2)) {
        std::cout << "G2 is a DAG" << std::endl;
    } else {
        std::cout << "G2 has a cycle" << std::endl;
    }

    //
//    Mip::Expr elsoKor;
//    elsoKor += Mip::colFromId(0);
//    elsoKor += Mip::colFromId(3);
//    elsoKor += Mip::colFromId(6);
//    elsoKor += Mip::colFromId(9);
//    elsoKor += Mip::colFromId(12);
//    mip.addRow(elsoKor <= 4);

//    mip.solve();

//    if (mip.type() == Mip::OPTIMAL) {
//        std::cout << "Objective function value: " << mip.solValue() << std::endl;
//
//        for (Mip::ColIt col(mip); col != INVALID; ++col) {
//            int colId = Mip::id(col);
//            std::cout << "x" << colId << "= " << mip.sol(Mip::colFromId(colId)) << std::endl;
//        }
//    } else {
//        std::cout << "Optimal solution not found." << std::endl;
//    }
}

void triangleInequalities(const Log &log, Mip &mip, std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int)) {
    for (int i = 0; i < n; ++i) {
        for (int j = i+1; j < n; ++j) {
            for (int k = j+1; k < n; ++k) {
                if (j == i || k == i || k == j) continue;
                Mip::Col y_ij = y[i][j];
                Mip::Col y_ik = y[i][k];
                Mip::Col y_jk = y[j][k];

                if (isLog(Debug, log)) {
//                    std::cout << "  col[" << Mip::id(y_ij) << "] + col[" << Mip::id(y_jk) << "] - col[" << Mip::id(y_ik)
//                              << "] <= 1" << std::endl;
                    std::cout << "  " << writer(i,j) << " + " << writer(j,k) << " - " << writer(i,k) << " <= 1" << std::endl;
                }
                mip.addRow(y_ij + y_jk - y_ik <= 1);

                if (isLog(Debug, log)) {
//                    std::cout << "  col[" << Mip::id(y_ik) << "] - col[" << Mip::id(y_ij) << "] - col[" << Mip::id(y_jk)
//                              << "] <= 0" << std::endl;
                    std::cout << "  " << writer(i,k) << " - " << writer(i,j) << " - " << writer(j,k) << " <= 0" << std::endl;
                }
                mip.addRow(y_ik - y_ij - y_jk <= 0);
            }
        }
    }

    if (isLog(Debug, log)) {
        std::cout << std::endl;
    }
}

void init(SmartDigraph &g, int nodeSize, const Mip &mip, const std::vector<std::vector<Mip::Col> > &y) {
    g.reserveNode(nodeSize);

    for (int i = 0; i < nodeSize; ++i) {
        g.addNode();
    }

    for (int i = 0; i < nodeSize-1; ++i) {
        for (int j = i+1; j < nodeSize; ++j) {
            if (mip.sol(y[i][j]) == 1) {
//                g2.addArc(SmartDigraph::nodeFromId(i), SmartDigraph::nodeFromId(j));
                addArc(g, i, j);
            } else if (mip.sol(y[i][j]) == 0) {
//                g2.addArc(SmartDigraph::nodeFromId(j), SmartDigraph::nodeFromId(i));
                addArc(g, j, i);
            }
        }
    }
}

void init(const SmartDigraph &sourceGraph, SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map) {
    DIGRAPH_TYPEDEFS(SmartDigraph);
    int n = sourceGraph.nodeNum();
    g.reserveNode(n);

    for (int i = 0; i < n; ++i) {
        g.addNode();
    }

    for(ArcIt it(sourceGraph); it != INVALID; ++it) {
        double result = mip.sol(map[it]);
        if (result == 1) {
            Node source = sourceGraph.source(it);
            Node target = sourceGraph.target(it);
            addArc(g, source, target);
        }
    }
}

void addArc(SmartDigraph &g, int sourceNode, int targetNode) {
    SmartDigraph::Node s = SmartDigraph::nodeFromId(sourceNode);
    SmartDigraph::Node t = SmartDigraph::nodeFromId(targetNode);
    addArc(g, s, t);
}

void addArc(SmartDigraph &g, SmartDigraph::Node &source, SmartDigraph::Node &target) {
    g.addArc(source, target);
}

bool isDAG(const Log &log, SmartDigraph &g, const std::string &graphName) {
    bool isDag = dag(g);

    if (isDag) {
        if (isLog(Debug, log)) {
            std::cout << "  The new " << graphName << " graph is a DAG with " << g.arcNum() << " arcs" << std::endl;
        }
    } else {
        if (isLog(Info, log)) {
            std::cout << "Error: " << graphName << " graph is not a DAG!" << std::endl
                      << std::endl;
        }
    }

    return isDag;
}

void getArcsForTheFAS(const Log &log, const SmartDigraph &g, std::vector<SmartDigraph::Arc> &fas,
                      const SmartDigraph::NodeMap<int> &topOrder) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    for(ArcIt it(g); it != INVALID; ++it) {
        Node source = g.source(it);
        Node target = g.target(it);

        if (topOrder[source] < topOrder[target]) {
            if (isLog(Info, log)) {
                std::cout << " (" << SmartDigraph::id(source) + 1 << "," << SmartDigraph::id(target) + 1 << ")";
            }

//            fas.push_back(SmartDigraph::Arc(it));// INFO: így copy-zom az élt
            fas.push_back(it);// INFO: lehet így is copy-zom az élt a vector-ban?
        }
    }

    if (isLog(Info, log)) {
        std::cout << std::endl;
    }
}

std::string wr_y(int i, int j) {
    return "y_(" + std::to_string(i+1) + "," + std::to_string(j+1) + ")";
}

std::string wr_y2(int i, int j) {
    return "y'_(" + std::to_string(i+1) + "," + std::to_string(j+1) + ")";
}

std::string wr_y3(int i, int j) {
    return "y\"_(" + std::to_string(i+1) + "," + std::to_string(j+1) + ")";
}

void wr_y(const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int)) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            std::cout << "  " << writer(i,j) << "= col[" << Mip::id(y[i][j]) << "]";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void wr_sol(Mip &mip, const std::vector<std::vector<Mip::Col> > &y, int n, std::string (*writer) (int, int),
            bool upperTriangMtx) {
    std::string placeholder;
    if (upperTriangMtx) {
        for (int i = 0; i < writer(n,n).size(); ++i) {
            placeholder += "-";
        }
        placeholder += "-----";
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (upperTriangMtx) {
                if (i < j) {
                    std::cout << "  " << writer(i,j) << "= " << mip.sol(y[i][j]);
                } else {
                    std::cout << placeholder;
                }
            } else {
                std::cout << "  " << writer(i,j) << "= " << mip.sol(y[i][j]);
            }
        }
        std::cout << std::endl;
    }
}

void wr_topOrder(const SmartDigraph::NodeMap<int> &topOrder, int n, const std::string &graphName) {
    std::cout << "  Topological order of " << graphName << ":";
    for (int i = 0; i < n; ++i) {
        int id = 0;
        while (i != topOrder[SmartDigraph::nodeFromId(id)] && id < n) {
            ++id;
        }
        std::cout << " " << id + 1;
    }
    std::cout << std::endl;
}

void wr_arcMap(const SmartDigraph &g, const Mip &mip, const SmartDigraph::ArcMap<Mip::Col> &map,
               const std::string &setName) {
    DIGRAPH_TYPEDEFS(SmartDigraph);

    std::cout << setName << ":";
    for (ArcIt it(g); it != INVALID; ++it) {
        double result = mip.sol(map[it]);
        if (result == 1) {
            std::cout << " " << SmartDigraph::id(it);
        }
    }
    std::cout << std::endl;
}
